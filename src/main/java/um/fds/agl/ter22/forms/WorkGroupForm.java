package um.fds.agl.ter22.forms;

public class WorkGroupForm {

    private long id;
    private String nomGroup;
    private String students;

    public WorkGroupForm(String nomGroup, String students) {
        this.nomGroup = nomGroup;
        this.students = students;
    }

    public WorkGroupForm(long id, String nomGroup, String students) {
        this.id = id;
        this.nomGroup = nomGroup;
        this.students = students;
    }

    public WorkGroupForm(long id, String nomGroup) {
        this.id = id;
        this.nomGroup = nomGroup;

    }

    public WorkGroupForm() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomGroup() {
        return nomGroup;
    }

    public void setNomGroup(String nomGroup) {
        this.nomGroup = nomGroup;
    }

    public String getStudents() {
        return students;
    }

    public void setStudents(String students) {
        this.students = students;
    }
}


