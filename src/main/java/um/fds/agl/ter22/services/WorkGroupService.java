package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.WorkGroup;
import um.fds.agl.ter22.repositories.SubjectRepository;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.repositories.WorkGroupRepository;

import java.util.Optional;

@Service
public class WorkGroupService {

    @Autowired
    private WorkGroupRepository workGroupRepository;


    public Iterable<WorkGroup> getWorkGroup() {
        return workGroupRepository.findAll();
    }

    public void deleteWorkGroup(final Long id) {
        workGroupRepository.deleteById(id);
    }



    public WorkGroup saveWorkGroup(WorkGroup workGroup) {
        WorkGroup savedWorkGroup = workGroupRepository.save(workGroup);
        return savedWorkGroup;
    }

    public Optional<WorkGroup> findById(long id) {
        return workGroupRepository.findById(id);
    }
}
