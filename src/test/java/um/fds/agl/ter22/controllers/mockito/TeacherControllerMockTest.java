package um.fds.agl.ter22.controllers.mockito;


import org.aspectj.lang.annotation.Before;
import org.hibernate.service.spi.InjectService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import um.fds.agl.ter22.controllers.TeacherController;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.TeacherForm;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.services.TeacherService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)

public class TeacherControllerMockTest {

    private MockMvc mvc;
    @InjectMocks
    private TeacherForm teacherForm;




    @MockBean
    private TeacherService teacherService;


    @Captor
    ArgumentCaptor<Teacher> teacherCaptor;



    @BeforeEach
    public void setUp() {
        this.mvc = MockMvcBuilders.standaloneSetup(new TeacherController()).build();
        this.teacherService = new TeacherService();


    }

    @Test
    void addTeacherPostNonExistingTeacher() throws Exception {

        when(teacherService.findById(teacherForm.getId())).thenReturn(Optional.empty());

       //il y aura un teacherService.saveTeacher(t) : mais par defaut ça ne le fera pas

        this.mvc.perform(post("/addTeacher")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();

        //teacherController.addTeacher(teacherForm);


        verify(teacherService, atLeastOnce()).saveTeacher(teacherCaptor.capture());
        Teacher capturedTeacher = teacherCaptor.getValue();
        assertEquals("Kermarrec", capturedTeacher.getLastName());

    }





}
